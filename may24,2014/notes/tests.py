from notes import *
import pickle
import sys,traceback
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET

def run_notes_tests():
    def test_reset_note():
        reset_note()        
        print "Resetting the note"
        assert(getnote() == None)
    
    def test_add_note():
        print "\n\nAdd new note."
        title = raw_input("Enter title : ")
        body = raw_input("Enter body : ")
        add_new_note(title,body)
        
    def test_find_note():
        print "\n\nFind note."
        keyword = raw_input("Enter any keyword for finding note :")        
        find = find_note(keyword)

    def test_delete_note():
        print "\n\nDelete note(by title)."
        title = raw_input("Enter title of note to be deleted : ")
        delete_note(title)
      
    def test_edit_note():
        print "\n\nEdit note."
        keyword = raw_input("Enter any keyword for finding note :")        
        find = find_note(keyword)
        if find:
            print "Adding new node : "
            add_new_note("new note","csabdlsdv")
            print "Deleting searched node : "
            delete_note_key(keyword)
            print "Notes after Editing : ", getnote()

    def test_save_note_pickle():
        save_note_pickle()
        print "\n\nNotes Saved(Pickle)."
        listn = pickle.load( open( "testfile", "rb" ) )
        print "Displaying Saved Notes."
        print listn

    def test_save_note_xml():
        save_note_xml()
        
    test_add_note()
    test_add_note()
    test_add_note()
    test_find_note()
    test_delete_note()
    test_edit_note()
    test_save_note_pickle()
    test_save_note_xml()

run_notes_tests()
