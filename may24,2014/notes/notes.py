import pickle
import sys,re
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET

master_notes = []

#Add a new note
def add_new_note(title,body):
    global __note__
    global master_notes
    if title == '': 
        title = "title"
        for i in range(1,):
            title +=`i+1`
    note = (title,body)
    master_notes.append(note)
    print "New List : ",master_notes
    return 

#Delete a note(title)
def delete_note(title):
    global __note__
    global master_notes
    for i in range(0,len(master_notes)-1):
        if re.search(title, master_notes[i][0],re.IGNORECASE):
            master_notes.remove(master_notes[i])
    print "New List : ",master_notes
    return master_notes

#Delete a note(key)
def delete_note_key(key):
    global __note__
    global master_notes
    for i in master_notes:
        if re.search(key,i[0],re.IGNORECASE) or re.search(key,i[1],re.IGNORECASE):
            master_notes.remove(i)
    print "New List : ",master_notes
    return master_notes

#Find a note
def find_note(key):
    global master_notes
    global temp
    temp = []
    for i in master_notes:
        if re.search(key,i[0],re.IGNORECASE) or re.search(key,i[1],re.IGNORECASE):
            temp.append(i)
    print "List = ",temp
    return temp

#Get note
def getnote():
    global master_notes
    return master_notes

#Reset note
def reset_note():
    global master_notes
    global temp
    master_notes = []
    temp = []
    return master_notes

#ave notes Pickle
def save_note_pickle():
    global master_notes
    file_Name = "testfile"
    fileObject = open(file_Name,"wb") 
    pickle.dump(master_notes,fileObject)   
    fileObject.close()
    return

#Save notes XML
def save_note_xml():
    global master_notes
    root = ET.Element("Master_Note")
    for x in master_notes:
        doc = ET.SubElement(root, "Note")
        field1 = ET.SubElement(doc, "Title")
        field1.text = x[0]
        field2 = ET.SubElement(doc, "Body")
        field2.text = x[1]
    tree = ET.ElementTree(root)
    tree.write("Notes.xml")    
