import sys
import tempfile
import os
import Image
import commands

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__blob1__ = None
__constsize__ = 15 


def copytext(text):
    global __text__
    global temp
    if len(text) <= __constsize__:
        __text__ = text
    else:
        temp = tempfile.TemporaryFile("r+w")
        try:
            temp.write(text)
            temp.seek(0)
            print temp.read()
        finally:
            temp.close()

def copyblob(blob):
    global __blob__
    __blob__ = blob
    
def copyblob1(blob1):
    global __blob1__
    __blob1__ = blob1


def gettext():
    global __text__
    global f
    f = open("temp", "r")
    x = f.read()
    if x != None: 
        return x
    else:
        print __text__
        return __text__

def getblob():
    global __blob__
    return __blob__

def getblob1():
    global __blob1__
    print __blob1__
    return __blob1__

def getsize():
    global __size__
    return __size__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None


##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
