# -*- coding: UTF-8 -*-
import clipboard
import Image
import base64

__constsize__ = 15 
__constimg__ = 15

def run_clipboard_tests():
    def test_empty_clipboard():
        print "Initial State : text = null, blob = null"
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        print "Reset State : text = null, blob = null"
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = 'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        if len(text) <= __constsize__:
            print "Hindi Text State : text != null, blob = null"
            assert(msg == text)
            assert(clipboard.getblob() == None)
            print "Length of text : ", len(text)
            print "Hindi Text copied to clipboard"
        else:
            print "Error: Size to copy is larger than buffer"
            test_tempfile(msg)

    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")        
	text = clipboard.gettext()
        if len(text) <= __constsize__:
            print "English Text State : text != null, blob = null"
            assert(text == "hello, world!")
	    assert(clipboard.getblob() == None)
            print "Length of text : ",len(text)
            print "English Text copied to clipboard"
        else:
            print "Error: Size to copy is larger than buffer"
            test_tempfile(msg)
            
    def test_copy_blob():
        clipboard.reset()
        blob1=clipboard.getblob()
        list1 = ["surendra", "is", "a", "good", "programmer"]
        Str1 = ' '.join(list1)
        print Str1
	if len(Str1) <= __constimg__:
            print "Blob State : text = null, blob != null"
            assert(blob1 == Str1)
            assert(clipboard.gettext() == None)
            print "Blob copied to clipboard"
	else:
            print "Error: Size to copy is larger than buffer"
            test_tempfile(Str1)

    def test_copy_blobntext():
        clipboard.reset()
        clipboard.copytext("hello, world!")        
	with open("test.jpg", "rb") as imageFile:
            image = base64.b64encode(imageFile.read())
            print "Length of image :",len(image)
	clipboard.copyblob(image)
        text = clipboard.gettext()
        blob = clipboard.getblob()
        if len(image) <= __constimg__ and len(text) <= __constsize__:
            print "Text and Blob both are copied"
	    assert(text == "hello, world!")
            assert(blob == image)
	elif len(image) > __constimg__ and len(text) <= __constsize__:
            print "Text copied and Blob not"
	    assert(text == "hello, world!")
            print "Error: Size of blob is larger than buffer"
            test_tempfile(blob)
        elif len(image) <= __constimg__ and len(text) > __constsize__:
            print "Blob copied and Text not"
	    assert(blob == image)
	    print "Error: Size of text is larger than buffer"
            test_tempfile(text)
        else:
            print "Both Blob and Text are not coplied on clipboard" 
            print "Error: Size of blob and text copy is larger than buffer"
            test_tempfile(text)
 	    test_tempfile(blob)

    def test_tempfile( t ):
        try:
            filetemp = open("tempfile","a+")
            filetemp.write(t)
            filetemp.write("\n\n\n")
        except:
            print "Error : Content not copied to the temporary file"
        else:
            print "Content copied to temporary file"       
            filetemp.close()

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_hindi_text()
    test_copy_english_text()
    test_copy_blob()
    test_copy_blobntext()

run_clipboard_tests()

def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
        with open("test.jpg", "rb") as imageFile:
            image = base64.b64encode(imageFile.read())
            print len(image)
        blob= clipboard.copyblob(image)       
        
        list1 = ["surendra", "is", "a", "good", "programmer"]
        Str1 = ' '.join(list1)
        blob1 = clipboard.copyblob(Str1)

    test_one_observer()

run_clipboard_observer_tests()
