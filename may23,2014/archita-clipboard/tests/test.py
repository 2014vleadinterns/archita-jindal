# -*- coding: UTF-8 -*-
import clipboard
import Image
import base64
import sys
import binascii

def run_clipboard_tests():
    def test_empty_clipboard():
        print "Initial State : text = null, blob = null"
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        print "Reset State : text = null, blob = null"
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = 'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        print "Hindi Text State : text != null, blob = null"
        print text
        assert(msg == text)
        assert(clipboard.getblob() == None)
            
    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")        
	text = clipboard.gettext()
        print "English Text State : text != null, blob = null"
        assert(text == "hello, world!")
	assert(clipboard.getblob() == None)

    def test_copy_text_loop():
        text = clipboard.gettext();
        print "Previous Text : ",text
        clipboard.reset()
        clipboard.copytext("hieee, world!")        
	text = clipboard.gettext()
        assert(text == "hieee, world!")
        print "New Text : "
	assert(clipboard.getblob() == None)
        print "English Text copied to clipboard"

    def test_copy_blob():
        clipboard.reset()
        list1 = ["surendra", "is", "a", "good", "programmer"]
        Str1 = ' '.join(list1)
        print Str1
        print "Blob State : text = null, blob != null"
        clipboard.copyblob1(Str1)
        blob1 = clipboard.getblob1()
        print blob1
        assert(blob1 == Str1)
        assert(clipboard.gettext() == None)
        print "Blob copied to clipboard"
	
    def test_copy_blobntext():
        clipboard.reset()
        clipboard.copytext("hello, world!")        
	with open("test.png", "rb") as imageFile:
            image = base64.b64encode(imageFile.read())
        clipboard.copyblob(image)
        text = clipboard.gettext()
        blob = clipboard.getblob()
        assert(text == "hello, world!")
        assert(blob == image)

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_hindi_text()
    test_copy_english_text()
    test_copy_text_loop()
    test_copy_blob()
    test_copy_blobntext()

run_clipboard_tests()

def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
        with open("test.png", "rb") as imageFile:
            image = base64.b64encode(imageFile.read())
            print len(image)
        blob= clipboard.copyblob(image)       
        
        list1 = ["surendra", "is", "a", "good", "programmer"]
        Str1 = ' '.join(list1)
        blob1 = clipboard.copyblob(Str1)

    test_one_observer()

run_clipboard_observer_tests()
