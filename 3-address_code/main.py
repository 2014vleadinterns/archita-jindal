import sys
import re

op = ['+','-','*','/','>>','<<']

def check(l):
    #Check l[0]
    if l[0] not in op:
        print "Invalid Operator : ",l[0]
    else:
        print "Valid Operator : ",l[0]

    #Check l[1]       
    if re.match('([_]*[a-zA-Z][a-zA-Z0-9_]*$)', l[1]) or re.match('([a-zA-Z][a-zA-Z0-9_]*$)', l[1]) or re.match('([-][0-9]*$)', l[1]) or re.match('[0-9]*$',l[1]):
        print "Valid Operand1 : ",l[1]
    else:
        print "Invalid Operand1 : ",l[1]
   
    #Check l[2]
    if re.match('([_]*[a-zA-Z][a-zA-Z0-9_]*$)', l[2]) or re.match('([a-zA-Z][a-zA-Z0-9_]*$)', l[2]) or re.match('([-][0-9]*$)', l[2]) or re.match('[0-9]*$',l[2]):
        print "Valid Operand2 : ",l[2]
    else:
        print "Invalid Operand2 : ",l[2]
   
    #Check l[3]
    if re.match('([_]*[a-zA-Z][a-zA-Z0-9_]*$)', l[3]) or re.match('([a-zA-Z][a-zA-Z0-9_]*$)', l[3]):
        print "Valid Result : ",l[3]
    else:
        print "Invalid Result : ",l[3]

####
fo = open("input", "rw+")
count = 0
with open('input') as infp:
    for line in infp:
        if line.strip():
            count += 1
#print 'number of non-blank lines found %d' % count
line = fo.readline()
for line in open('input'):
    l = line.split()
    print "\n\nList", l 
    check(l)
fo.close()
