# -*- coding: UTF-8 -*-
import clipboard
import Image

__constsize__ = 20
__constimg__ = 1024

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copy_hindi_text():
        clipboard.reset()
        msg = u'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        clipboard.copytext(msg)
        text = clipboard.gettext()
        #size = clipboard.getsize()
        try:
            len(text) <= __constsize__
            
        except IOError:
            print "Error: Size to copy is larger than buffer"
        else:
            assert(msg == text)
            print "Content copied to clipboard"

    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")
        text = clipboard.gettext()
        #size = clipboard.getsize()
        try:
            len(text) <= __constsize__
        except IOError:
            print "Error: Size to copy is larger than buffer"
        else:
            assert(text == "hello, world!")
            print len(text)
            print "Content copied to clipboard"

    def test_copy_image():
        clipboard.reset()
        image = Image.open("test.png")
		width,height = im.size
		reso = height*width
		if reso <= __constimg__:
		    print reso
			print "Image copied to clipboard"
		else:
			print "Error: Size to copy is larger than buffer"

    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    #test_copy_hindi_text()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
        image = Image.open("url.jpeg")
    
    test_one_observer()

run_clipboard_observer_tests()
