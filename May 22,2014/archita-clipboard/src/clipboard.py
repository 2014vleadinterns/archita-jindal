import sys

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__size__ = None

def copytext(text):
    global __text__
    global __sizet__
    __text__ = text
    __size__ = len(text)

def copyblob(blob):
    global __blob__
    __blob__ = blob

def gettext():
    global __text__
    return __text__

def getblob():
    global __blob__
    return __blob__

def getsize():
    global __size__
    return __size__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None
    __size__ = None


##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
